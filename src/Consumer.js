import React, { useEffect, useState } from "react";
import Webcam from "react-webcam";
import io from 'socket.io-client';
import { useSearchParams } from 'react-router-dom';

const MDS = require('mediasoup-client');
const Peer = require('./peer');
const SocketQueue = require('./queue');


let peer;
const queue = new SocketQueue();

var socket = null;
let activate = false
function Consumer() {
    const [searchParams] = useSearchParams();
    const [enabled, setEnabled] = useState(false);
    const [isRecording, setRecording] = useState(false);
    const [isReady, setIsReady] = useState(false);
    const [producer_id, setProducerId] = useState(searchParams.get('producer'));
    var audio_ref = null
    useEffect(() => {

        if (activate) {
            //   console.log("SETUP NE")
            //   socket = io(`ws://localhost:9999?side=consume&producer=${searchParams.get('producer')}`);
            //   console.log("SocketListerner")
            //   socket.on('open', (message) => handleSocketOpen(message));
            //   socket.on('router-rtp-capabilities', (message) => handleRouterRtpCapabilitiesRequest(message));
            //   socket.on('create-transport', (message) => handleCreateTransportRequest(message));
            //   socket.on('connect-transport', (message) => handleConnectTransportRequest(message));
            //   socket.on('produce', (message) => {
            //     handleProduceRequest(message)}
            //   );

            //   socket.on('error', (message) => handleSocketError(message));
            //   socket.on('close', (message) => handleSocketClose(message));
        } else {
            activate = true
        }


    }, [])
    const subcribeToProduce = () => {
        try {
            console.log(producer_id)
            socket = io(`ws://192.168.150.19:9999?side=consume&clientId=SOCKET_2&producer=${searchParams.get('producer')}`);
            socket.on('router-rtp-capabilities', (message) => handleRouterRtpCapabilitiesRequest(message));
            socket.on('create-transport', (message) => handleCreateTransportRequest(message));
        } catch (e) {
            console.error(e)
        }
    }
    const handleSocketOpen = async () => {
        console.log('handleSocketOpen()');
    };


    const handleSocketClose = () => {
        console.log('handleSocketClose()');
        setEnabled(false)
    };

    const getVideoCodecs = () => {
        // const params = new URLSearchParams(location.search.slice(1));
        // const videoCodec = params.get('videocodec')
        // console.warn('videoCodec');

        // const codec = MDSConfig.router.mediaCodecs.find(c => {
        //   if (!videoCodec)
        //     return undefined;

        //   return ~c.mimeType.toLowerCase().indexOf(videoCodec.toLowerCase())
        // });

        // console.warn('codec', codec);
        // return codec ? codec : {
        //   kind: 'video',
        //   mimeType: 'video/VP8',
        //   clockRate: 90000,
        //   parameters: {
        //     'x-google-start-bitrate': 1000
        //   }
        // };
    }


    const handleSocketError = error => {
        console.error('handleSocketError() [error:%o]', error);
    };


    const handleRouterRtpCapabilitiesRequest = async (jsonMessage) => {
        const { routerRtpCapabilities, sessionId } = jsonMessage;
        console.log('handleRouterRtpCapabilities() [rtpCapabilities:%o]', routerRtpCapabilities);
        console.log("handleRouterRtpCapabilitiesRequest sessionId ", sessionId)
        try {
            const device = new MDS.Device();
            await device.load({ routerRtpCapabilities });
            peer = new Peer(sessionId, device);
            setIsReady(true)
            createTransport();
        } catch (error) {
            console.error('handleRouterRtpCapabilities() failed to init device [error:%o]', error);
            socket.close();
        }
    };

    const createTransport = () => {
        console.log('createTransport()');

        if (!peer || !peer.device.loaded) {
            throw new Error('Peer or device is not initialized');
        }

        // First we must create the MDS transport on the server side
        socket.emit('create-producer-transport', {
            sessionId: peer.sessionId
        });
    };

    //Transport on the server side has been created
    const handleCreateTransportRequest = async (jsonMessage) => {
        console.log('handleCreateTransportRequest() [data:%o]', jsonMessage);

        try {
            // Create the local receiver transport
            peer.receiveTransport = await peer.device.createRecvTransport(jsonMessage);
            console.log('handleCreateTransportRequest() receive transport created [id:%s]', peer.receiveTransport.id);
            // Set the transport listeners and get the users media stream
            handleSendTransportListeners();

            const { rtpCapabilities } = peer.device;
            console.log("rtpCapabilities ", rtpCapabilities)
            const transportId = peer.receiveTransport.id;
            console.log("Consume called for audio conference")
            socket.emit('consume', { clientId: 'SOCKET_2', rtpCapabilities, producer_id, transportId, dataChannel: false }, async (response) => {
                if (response) {
                    console.log("REsponse ", response)
                    const {
                        producerId,
                        id,
                        kind,
                        rtpParameters,
                    } = response;
                    let codecOptions = {};
                    console.log("check one ")
                    const consumer = await peer.receiveTransport.consume({
                        id,
                        producerId,
                        kind,
                        rtpParameters,
                        codecOptions,
                    });
                    consumer.resume()
                    
                    const stream = new MediaStream();
                    stream.addTrack(consumer.track);
                    console.log("stream = ", stream)
                    audio_ref.srcObject = stream;
                    console.log(audio_ref.srcObject)
                    console.log("check two ", await consumer.getStats())
                } else {
                    console.log("Consume Error ")
                }

            });


        } catch (error) {
            console.error('handleCreateTransportRequest() failed to create transport [error:%o]', error);
            socket.close();
        }
    };

    const handleSendTransportListeners = () => {
        peer.receiveTransport.on('connect', handleTransportConnectEvent);
        // peer.receiveTransport.on('produce', handleTransportProduceEvent);
        peer.receiveTransport.on('connectionstatechange', connectionState => {
            console.log('RV transport connection state change [state:%s]', connectionState);
        });
    };


    const handleConnectTransportRequest = async (jsonMessage) => {
        console.log('handleTransportConnectRequest()');
        try {
            const action = queue.get('connect-transport');

            if (!action) {
                throw new Error('transport-connect action was not found');
            }

            await action(jsonMessage);
        } catch (error) {
            console.error('handleTransportConnectRequest() failed [error:%o]', error);
        }
    };

    const handleProduceRequest = async (jsonMessage) => {
        console.log('handleProduceRequest()');
        try {
            const action = queue.get('produce');
            if (!action) {
                throw new Error('produce action was not found');
            }

            await action(jsonMessage);
        } catch (error) {
            console.error('handleProduceRequest() failed [error:%o]', error);
        }
    };

    const handleTransportConnectEvent = ({ dtlsParameters }, callback, errback) => {
        try {
            // const action = (jsonMessage) => {
            //     callback();
            //     queue.remove('connect-transport');
            // };
            // queue.push('connect-transport', action);
            socket.emit("create-consumer-connect", {
                sessionId: peer.sessionId,
                transportId: peer.receiveTransport.id,
                dtlsParameters
            }, ()=>{
                callback()
            })
        } catch (error) {
            console.error('handleTransportConnectEvent() failed [error:%o]', error);
            errback(error);
        }
    };

    const handleTransportProduceEvent = ({ kind, rtpParameters }, callback, errback) => {
        console.log('handleTransportProduceEvent()');
        try {
            const action = (jsonMessage) => {
                console.log('handleTransportProduceEvent callback [data:%o]', jsonMessage);
                callback({ id: jsonMessage.id });
                queue.remove('produce');
            };
            queue.push('produce', action);


            socket.emit("produce", {
                sessionId: peer.sessionId,
                transportId: peer.receiveTransport.id,
                kind,
                rtpParameters
            })
        } catch (error) {
            console.error('handleTransportProduceEvent() failed [error:%o]', error);
            errback(error);
        }
    };


    const startRecord = () => {
        console.log('startRecord()');
        setRecording(true)
        socket.emit("start-record", {
            sessionId: peer.sessionId
        })
    };

    const stopRecord = () => {
        console.log('stopRecord()');
        setRecording(false)
        socket.emit("stop-record", {
            sessionId: peer.sessionId
        })
    };

    const subcribe = () => {
        console.log('Subcribe to producer');

        socket.emit("subcribe", {
            sessionId: peer.sessionId
        })
    };



    return (
        <div className="App">
            {/* {isReady && <Webcam
        audio={true}
        mirrored={true}
        onUserMedia={async (mediaStream) => {
          // // Get the video and audio tracks from the media stream
          const videoTrack = mediaStream.getVideoTracks()[0];
          const audioTrack = mediaStream.getAudioTracks()[0];
          console.log("ConChimCu", videoTrack)
          // // If there is a video track start sending it to the server
          if (videoTrack) {
            const videoProducer = await peer.receiveTransport.produce({ track: videoTrack });
            peer.producers.push(videoProducer);
          }

          // // if there is a audio track start sending it to the server
          if (audioTrack) {
            const audioProducer = await peer.receiveTransport.produce({ track: audioTrack });
            peer.producers.push(audioProducer);
          }
          console.log("Cho phep quay video")
          //Enable the start record button
          setEnabled(true)
        }}
      />} */}
            <div>Video</div>
            <video ref={audio => { audio_ref = audio }} controls volume="true" autoPlay={true} playsInline />


            <div>
                <button
                    onClick={() => {
                        subcribeToProduce()
                    }}
                >
                    Subcribe
                </button>
            </div>
        </div>
    );
}

export default Consumer;
