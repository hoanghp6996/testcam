import React, { useEffect, useState } from "react";
import Webcam from "react-webcam";
import io from 'socket.io-client';

const MDS = require('mediasoup-client');
const Peer = require('./peer');
const SocketQueue = require('./queue');


let peer;
const queue = new SocketQueue();

const socket = io(`wss://127.0.0.1:9999?side=producer&clientId=SOCKET_3`);
let activate = false
function App() {

  const [enabled, setEnabled] = useState(false);
  const [isRecording, setRecording] = useState(false);
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {

    if (activate) {
      console.log("SocketListerner")
      socket.on('connected', (message) => handleSocketOpen(message));
      socket.on('router-rtp-capabilities', (message) => handleRouterRtpCapabilitiesRequest(message));
      socket.on('create-transport', (message) => handleCreateTransportRequest(message));
      socket.on('connect-transport', (message) => handleConnectTransportRequest(message));
      socket.on('produce', (message) => {
        handleProduceRequest(message)}
      );

      socket.on('error', (message) => handleSocketError(message));
      socket.on('close', (message) => handleSocketClose(message));
    } else {
      activate = true
    }


  }, [])

  const handleSocketOpen = async () => {
    console.log('handleSocketOpen()', socket);
  };


  const handleSocketClose = () => {
    console.log('handleSocketClose()');
    setEnabled(false)
  };

  const getVideoCodecs = () => {
    // const params = new URLSearchParams(location.search.slice(1));
    // const videoCodec = params.get('videocodec')
    // console.warn('videoCodec');

    // const codec = MDSConfig.router.mediaCodecs.find(c => {
    //   if (!videoCodec)
    //     return undefined;

    //   return ~c.mimeType.toLowerCase().indexOf(videoCodec.toLowerCase())
    // });

    // console.warn('codec', codec);
    // return codec ? codec : {
    //   kind: 'video',
    //   mimeType: 'video/VP8',
    //   clockRate: 90000,
    //   parameters: {
    //     'x-google-start-bitrate': 1000
    //   }
    // };
  }


  const handleSocketError = error => {
    console.error('handleSocketError() [error:%o]', error);
  };


  const handleRouterRtpCapabilitiesRequest = async (jsonMessage) => {
    const { routerRtpCapabilities, sessionId } = jsonMessage;
    console.log('handleRouterRtpCapabilities() [rtpCapabilities:%o]', routerRtpCapabilities);

    try {
      const device = new MDS.Device();
      await device.load({ routerRtpCapabilities });
      peer = new Peer(sessionId, device);
      console.log("PEER CREATED")
      setIsReady(true)
      createTransport();
    } catch (error) {
      console.error('handleRouterRtpCapabilities() failed to init device [error:%o]', error);
      socket.close();
    }
  };

  const createTransport = () => {
    console.log('createTransport()');

    if (!peer || !peer.device.loaded) {
      throw new Error('Peer or device is not initialized');
    }

    // First we must create the MDS transport on the server side
    socket.emit('create-producer-transport', {
      sessionId: peer.sessionId
    });
  };

  //Transport on the server side has been created
  const handleCreateTransportRequest = async (jsonMessage) => {
    console.log('handleCreateTransportRequest() [data:%o]', jsonMessage);

    try {
      // Create the local send transport
      peer.sendTransport = await peer.device.createSendTransport({
        ...jsonMessage, iceServers: [{
            'urls': 'stun:stun1.l.google.com:19302'
        }]
    });
      console.log('handleCreateTransportRequest() send transport created [id:%s]', peer.sendTransport.id);

      // Set the transport listeners and get the users media stream
      handleSendTransportListeners();
    } catch (error) {
      console.error('handleCreateTransportRequest() failed to create transport [error:%o]', error);
      socket.close();
    }
  };

  const handleSendTransportListeners = () => {
    peer.sendTransport.on('connect', handleTransportConnectEvent);
    peer.sendTransport.on('produce', handleTransportProduceEvent);
    peer.sendTransport.on('connectionstatechange', connectionState => {
      //   console.log('send transport connection state change [state:%s]', connectionState);
    });
  };


  const handleConnectTransportRequest = async (jsonMessage) => {
    console.log('handleTransportConnectRequest()');
    try {
      const action = queue.get('connect-transport');

      if (!action) {
        throw new Error('transport-connect action was not found');
      }

      await action(jsonMessage);
    } catch (error) {
      console.error('handleTransportConnectRequest() failed [error:%o]', error);
    }
  };

  const handleProduceRequest = async (jsonMessage) => {
    console.log('handleProduceRequest()');
    try {
      const action = queue.get('produce');
      if (!action) {
        throw new Error('produce action was not found');
      }

      await action(jsonMessage);
    } catch (error) {
      console.error('handleProduceRequest() failed [error:%o]', error);
    }
  };

  const handleTransportConnectEvent = ({ dtlsParameters }, callback, errback) => {
    try {
      console.log("Connected to the transport ", dtlsParameters)
      const action = (jsonMessage) => {
        console.log('connect-transport action');
        callback();
        queue.remove('connect-transport');
      };

      queue.push('connect-transport', action);
      socket.emit("create-producer-connect", {
        sessionId: peer.sessionId,
        transportId: peer.sendTransport.id,
        dtlsParameters
      })
    } catch (error) {
      console.error('handleTransportConnectEvent() failed [error:%o]', error);
      errback(error);
    }
  };

  const handleTransportProduceEvent = ({ kind, rtpParameters }, callback, errback) => {
    console.log('handleTransportProduceEvent()');
    try {
      const action = (jsonMessage) => {
        console.log('handleTransportProduceEvent callback [data:%o]', jsonMessage);
        callback({ id: jsonMessage.id });
        queue.remove('produce');
      };
      queue.push('produce', action);


      socket.emit("produce", {
        sessionId: peer.sessionId,
        transportId: peer.sendTransport.id,
        kind,
        rtpParameters
      })
    } catch (error) {
      console.error('handleTransportProduceEvent() failed [error:%o]', error);
      errback(error);
    }
  };


  const startRecord = () => {
    console.log('startRecord()');
    setRecording(true)
    socket.emit("start-record", {
      sessionId: peer.sessionId
    })
  };

  const stopRecord = () => {
    console.log('stopRecord()');
    setRecording(false)
    socket.emit("stop-record", {
      sessionId: peer.sessionId
    })
  };



  return (
    <div className="App">
      {isReady && <Webcam
        audio={true}
        mirrored={true}
        onUserMedia={async (mediaStream) => {
          // // Get the video and audio tracks from the media stream
          const videoTrack = mediaStream.getVideoTracks()[0];
          const audioTrack = mediaStream.getAudioTracks()[0];
          console.log("ConChimCu", videoTrack)
          // // If there is a video track start sending it to the server
          if (videoTrack) {
            const videoProducer = await peer.sendTransport.produce({ track: videoTrack });
            peer.producers.push(videoProducer);
          }

          // // if there is a audio track start sending it to the server
          if (audioTrack) {
            const audioProducer = await peer.sendTransport.produce({ track: audioTrack });
            peer.producers.push(audioProducer);
          }
          console.log("Cho phep quay video")
          //Enable the start record button
          setEnabled(true)
        }}
      />}
      <div>
        <button
          disabled={!enabled}
          onClick={() => {
            if (isRecording) {
              stopRecord()
            } else {
              startRecord()
            }
          }}
        >
          {isRecording ? "STOP" : "START"}
        </button>
      </div>
    </div>
  );
}

export default App;
