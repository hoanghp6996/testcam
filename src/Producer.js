import React, { useEffect, useState } from "react";
import Webcam from "react-webcam";
import socketClient from 'socket.io-client';
const socketPromise = require('./socket.io-promise').promise;

const MDS = require('mediasoup-client');

let socket;
let device;
let videoProducer, audioProducer, sendTransport;

let activate = false
const constraints = {
  width: { min: 640, ideal: 1080, max: 1080 },
  height: { min: 400, ideal: 720 },
  frameRate: { max: 25 }
};

function App() {
  const [status, setStatus] = useState('Not connect');
  const [enabled, setEnabled] = useState(false);
  const [isRecording, setRecording] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [sessionID, setSessionID] = useState(null);

  useEffect(() => {
    if (activate) {
      console.log("Socket setup ")
      connect()
    } else {
      activate = true
    }


  }, [])



  const connect = async () => {
    // $btnConnect.disabled = true;
    // $txtConnection.innerHTML = 'Connecting...';

    const opts = {
    };

    const serverUrl = `https://streaming.evbi.vn?side=streamer&session_id=1234567789`; //https://foxsword.dev
    socket = socketClient(serverUrl, opts);
    socket.request = socketPromise(socket);

    socket.on('connect', async () => {
      const data = await socket.request('getRouterRtpCapabilities');
      console.log("connect ok", data)
      await loadDevice(data);
      setIsReady(true)
      setStatus('Connect to socket: OK')
    });
    socket.on('init_stream', async (data, callback) => {
      setSessionID(data.id)
    });
    socket.on('new_subscriber', async (data, callback) => {
      console.log("Total "+data.count+" viewers")
     
    });

    socket.on('disconnect', () => {
      console.log("Socket::Disconnected")
      disconnect()
      setStatus('Connect to socket: Disconnected')
    });

    socket.on('connect_error', (error) => {
      setStatus('Connect to socket: Error')
    });

    socket.on('newConsumer', () => {

    });
  }


  const streamToServer = async (mediaStream) => {
    const data = await socket.request('createProducerTransport', {
      forceTcp: false,
      rtpCapabilities: device.rtpCapabilities,
    });
    if (data.error) {
      console.error(data.error);
      return;
    }

    sendTransport = device.createSendTransport({
      ...data, iceServers: [{
          'urls': 'stun:171.244.53.109:3478'
        }]
    });
    sendTransport.on('connect', async ({ dtlsParameters }, callback, errback) => {
      socket.request('connectProducerTransport', { dtlsParameters })
        .then(callback)
        .catch(errback);
    });

    sendTransport.on('produce', async ({ kind, rtpParameters }, callback, errback) => {
      try {
        const { id } = await socket.request('produce', {
          transportId: sendTransport.id,
          kind,
          rtpParameters,
        });
        callback({ id });
      } catch (err) {
        errback(err);
      }
    });

    sendTransport.on('connectionstatechange', (state) => {
      switch (state) {
        case 'connecting':
            setStatus('Stream: publishing...') //Dang ket noi de stream
          break;

        case 'connected':
            socket.request('broadcasting').then(()=>{
              console.log("broadcasting....")
            })
            setStatus('Stream: published.') //ok va dang stream len
          break;

        case 'failed':
            disconnect()
            setStatus('Stream: Failed.')
          break;

        default: break;
      }
    });

    try {
    
      const videoTrack = mediaStream.getVideoTracks()[0];
      const audioTrack = mediaStream.getAudioTracks()[0];

      // if (chkSimulcast.checked) {
      //   params.encodings = [
      //     { maxBitrate: 100000 },
      //     { maxBitrate: 300000 },
      //     { maxBitrate: 900000 },
      //   ];
      //   params.codecOptions = {
      //     videoGoogleStartBitrate: 1000
      //   };
      // }

      if (videoTrack) {
         videoProducer = await sendTransport.produce({ track: videoTrack });
      }
      if (audioTrack) {
         audioProducer = await sendTransport.produce({ track: audioTrack });
      }

    } catch (err) {
      console.log(err)
      disconnect()
      setStatus('Stream: Failed.')
    }
  }


  const disconnect = ()=>{
    if(sendTransport){
      sendTransport.close()
    }
    setRecording(false)
  }

  const startRecord = async () => {
    setRecording(true)
    const data = await socket.request('start-record', {
      forceTcp: false
    });
    console.log("startRecord ", data)
   
  }
  const stopRecord = async () => {
    const data = await socket.request('stop-record', {
      forceTcp: false
    });
    console.log("stop record is ok")
    setRecording(false)
  }

  const loadDevice = async (routerRtpCapabilities) => {
    try {
      device = new MDS.Device();
    } catch (error) {
      if (error.name === 'UnsupportedError') {
        console.error('browser not supported');
      }
    }
    await device.load({ routerRtpCapabilities });
  }


  return (
    <div className="App">
      {isReady && <Webcam
        audio={true}
        // mirrored={true}
        videoConstraints={{
          width: { min: 640, ideal: 720, max: 720 },
          height: { min: 400, ideal: 640 },
          frameRate: { max: 30 },
          facingMode: { exact: "environment" }
      }}
        onUserMedia={async (mediaStream) => {
          console.log("set mediaStream")
          streamToServer(mediaStream);
          setEnabled(true)
        }}
      />}
      <div>
        <button
          disabled={!enabled}
          onClick={() => {
            if (isRecording) {
              stopRecord()
            } else {
              startRecord()
            }
          }}
        >
          {isRecording ? "STOP" : "START"}

        </button>
        <p>{status}</p>
        <p>SessionID: {sessionID}</p>
      </div>
    </div>
  );
}

export default App;
