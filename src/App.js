import logo from './logo.svg';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import Producer from "./Producer";
import Consumer from "./Consumer";
import './App.css';

function App() {
  return (
    <div className="App">

      <div>

        <Router>
          <Routes>
            <Route path="/consumer" element={<Consumer/>}/> 
            <Route exact path="/" element={<Producer />} />
           
          </Routes>
        </Router>
      </div>

    </div>
  );
}

export default App;
